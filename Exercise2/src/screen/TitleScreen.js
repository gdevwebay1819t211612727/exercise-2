import { Container, Text,TEXT_GRADIENT } from "pixi.js";

class TitleScreen extends Container{
    constructor(){
		super();
		
		const app = new PIXI.Application({
			"width":1000,
			"height":800,
			"view": document.getElementById("game_canvas"),
			"backgroundColor": 0x00FF00,
			"autoStart":true
		});
		

var idNumber = new Text("11612727",{
	fill: ['0xFFFFFF'],
	fontSize: 20
});
var fullName = new Text("Paolo V. Arevalo",{
	fill: ['0x551a8b','0x00FF00','0x0000FF','0xFFFF00'],
	fontSize: 20,
	fillGradientType: TEXT_GRADIENT.LINEAR_HORIZONTAL
});
var myProgram = new Text("Bachelor of Science in Interactive Entertainment and Multimedia Computing Major in Game Development",{
	fill: ['0xFF0000','0x0000FF','0xFFFF00'],
	fontSize: 18,
	fillGradientType: TEXT_GRADIENT.LINEAR_VERTICAL,
	fillGradientStops: [0.08,0.5]
});
var myCourse = new Text("Game Development for the Web",{
	fill: ['white'],
	fontSize: 18,
	wordWrap:true,
	wordWrapWidth:20
});
var lastNumber = new Text("I like to eat some Croissant",{
	fill: ['white'],
	fontSize: 30,
	fontFamily:'AirAmericana'
});
fullName.position.set(0,20);
myProgram.position.set(0,40);
myCourse.position.set(0,60);
lastNumber.position.set(0,200);
		
		var graphics = new PIXI.Graphics();
		var point1 = new PIXI.Point(225,475);
		var point2 = new PIXI.Point(250,400);
		var point3 = new PIXI.Point(275,475);
		var point4 = new PIXI.Point(215,420);
		var point5 = new PIXI.Point(285,420);
		var point6 = new PIXI.Point(225,475);
		
		var points = [point1,point2,point3,point4,point5,point6];
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.beginFill(0x0000FF);
		graphics.drawCircle(100,100,45);
		graphics.endFill();
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.beginFill(0xFF0000);
		graphics.drawRect(175,100,210,150);
		graphics.endFill();
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.beginFill(0x00FF00);
		graphics.drawEllipse(500,200,75,180);
		graphics.endFill();
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.drawCircle(625,100,45);
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.drawRect(700,100,210,150);
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.drawEllipse(100,400,75,180);
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.moveTo(225,275);
		graphics.lineTo(325,275);
		graphics.lineTo(325,375);
		graphics.lineTo(225,375);
		graphics.lineTo(225,275);
		
		graphics.lineStyle(4,0xFFFFFF);
		graphics.drawPolygon(points);
		
		//Ears Left
		graphics.lineStyle(4,0x00FF00);
		graphics.beginFill(0x00FF00);
		graphics.drawEllipse(400,500,15,50);
		graphics.endFill();
		//Ears Right
		graphics.lineStyle(4,0x00FF00);
		graphics.beginFill(0x00FF00);
		graphics.drawEllipse(475,500,15,50);
		graphics.endFill();
		//Head
		graphics.lineStyle(4,0xFFFF00);
		graphics.beginFill(0xFFFF00);
		graphics.drawCircle(438,585,60);
		graphics.endFill();
		//Eyes Left
		graphics.lineStyle(4,0xFF0000);
		graphics.beginFill(0xFF0000);
		graphics.drawCircle(415,570,8);
		graphics.endFill();
		//Eyes Right
		graphics.lineStyle(4,0xFF0000);
		graphics.beginFill(0xFF0000);
		graphics.drawCircle(460,570,8);
		graphics.endFill();
		//Mouth
		graphics.lineStyle(4,0x0000FF);
		graphics.moveTo(400,595);
		graphics.lineTo(475,595);
		//Teeth Left
		graphics.lineStyle(4,0x0000FF);
		graphics.beginFill(0xFFFFFF);
		graphics.drawRect(415,595,15,20);
		graphics.endFill();
		//Teeth Right
		graphics.lineStyle(4,0x0000FF);
		graphics.beginFill(0xFFFFFF);
		graphics.drawRect(445,595,15,20);
		graphics.endFill();
		
		
		this.addChild(graphics);

				this.addChild(idNumber);
				this.addChild(fullName);
				this.addChild(myProgram);
				this.addChild(myCourse);
				this.addChild(lastNumber);
				
				setTimeout(()=>{
        	lastNumber.text = "I like to eat some Croissant and some Baguette";
		},1000);
		
    }
}

export default TitleScreen;