export let AssetDirectory = {
	"load": [
		"assets/fonts/Air Americana.ttf",
		"assets/fonts/AirAmericana.eot",
		"assets/fonts/AirAmericana.svg",
		"assets/fonts/AirAmericana.woff",
		"assets/fonts/AirAmericana.woff2"
	],
	"audio": []
};