/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _TitleScreen = __webpack_require__(/*! ./screen/TitleScreen */ \"./src/screen/TitleScreen.js\");\n\nvar _TitleScreen2 = _interopRequireDefault(_TitleScreen);\n\nvar _ = __webpack_require__(/*! . */ \"./src/index.js\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Main = function () {\n    function Main() {\n        _classCallCheck(this, Main);\n    }\n\n    _createClass(Main, null, [{\n        key: \"start\",\n        value: function start() {\n            Main.cjAudioQueue = new createjs.LoadQueue();\n            createjs.Sound.alternateExtensions = [\"ogg\"];\n\n            Main.cjAudioQueue.installPlugin(createjs.Sound);\n            Main.cjAudioQueue.addEventListener(\"complete\", Main.handleAudioComplete.bind(Main));\n\n            if (_assetdirectory.AssetDirectory.audio.length > 0) {\n                //LOAD AUDIO                  \n                var audioFiles = _assetdirectory.AssetDirectory.audio;\n                var audioManifest = [];\n                for (var i = 0; i < audioFiles.length; i++) {\n                    audioManifest.push({\n                        id: audioFiles[i],\n                        src: audioFiles[i]\n                    });\n                }\n                Main.cjAudioQueue.loadManifest(audioManifest);\n            } else {\n                Main.handleAudioComplete();\n            }\n        }\n    }, {\n        key: \"handleAudioComplete\",\n        value: function handleAudioComplete() {\n            if (_assetdirectory.AssetDirectory.load.length > 0) {\n                //LOAD IMAGES         \n                var loader = _pixi.loaders.shared;\n                loader.add(_assetdirectory.AssetDirectory.load);\n                loader.load(Main.handleImageComplete);\n            } else {\n                Main.handleImageComplete();\n            }\n        }\n    }, {\n        key: \"handleImageComplete\",\n        value: function handleImageComplete() {\n            var screen = new _TitleScreen2.default();\n            _.App.stage.addChild(screen);\n        }\n    }]);\n\n    return Main;\n}();\n\nexports.default = Main;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvTWFpbi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvTWFpbi5qcz8xMjIxIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0RGlyZWN0b3J5IH0gZnJvbSBcIi4vYXNzZXRkaXJlY3RvcnlcIjtcclxuaW1wb3J0IHsgbG9hZGVycyB9IGZyb20gXCJwaXhpLmpzXCI7XHJcbmltcG9ydCBUaXRsZVNjcmVlbiBmcm9tIFwiLi9zY3JlZW4vVGl0bGVTY3JlZW5cIjtcclxuaW1wb3J0IHsgQXBwIH0gZnJvbSBcIi5cIjtcclxuXHJcblxyXG5jbGFzcyBNYWluIHtcclxuICAgIHN0YXRpYyBzdGFydCgpe1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlID0gbmV3IGNyZWF0ZWpzLkxvYWRRdWV1ZSgpO1xyXG4gICAgICAgIGNyZWF0ZWpzLlNvdW5kLmFsdGVybmF0ZUV4dGVuc2lvbnMgPSBbXCJvZ2dcIl07XHJcblxyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmluc3RhbGxQbHVnaW4oY3JlYXRlanMuU291bmQpO1xyXG4gICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmFkZEV2ZW50TGlzdGVuZXIoXCJjb21wbGV0ZVwiLCBNYWluLmhhbmRsZUF1ZGlvQ29tcGxldGUuYmluZChNYWluKSk7XHJcblxyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmF1ZGlvLmxlbmd0aCA+IDApe1xyXG4gICAgICAgICAgICAvL0xPQUQgQVVESU8gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGF1ZGlvRmlsZXMgPSBBc3NldERpcmVjdG9yeS5hdWRpbztcclxuICAgICAgICAgICAgbGV0IGF1ZGlvTWFuaWZlc3QgPSBbXTtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGF1ZGlvRmlsZXMubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgYXVkaW9NYW5pZmVzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogYXVkaW9GaWxlc1tpXSxcclxuICAgICAgICAgICAgICAgICAgICBzcmM6IGF1ZGlvRmlsZXNbaV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIE1haW4uY2pBdWRpb1F1ZXVlLmxvYWRNYW5pZmVzdChhdWRpb01hbmlmZXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVBdWRpb0NvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaGFuZGxlQXVkaW9Db21wbGV0ZSgpe1xyXG4gICAgICAgIGlmKEFzc2V0RGlyZWN0b3J5LmxvYWQubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgIC8vTE9BRCBJTUFHRVMgICAgICAgICBcclxuICAgICAgICAgICAgbGV0IGxvYWRlciA9IGxvYWRlcnMuc2hhcmVkO1xyXG4gICAgICAgICAgICBsb2FkZXIuYWRkKEFzc2V0RGlyZWN0b3J5LmxvYWQpO1xyXG4gICAgICAgICAgICBsb2FkZXIubG9hZChNYWluLmhhbmRsZUltYWdlQ29tcGxldGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgTWFpbi5oYW5kbGVJbWFnZUNvbXBsZXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBoYW5kbGVJbWFnZUNvbXBsZXRlKCl7XHJcbiAgICAgICAgbGV0IHNjcmVlbiA9IG5ldyBUaXRsZVNjcmVlbigpO1xyXG4gICAgICAgIEFwcC5zdGFnZS5hZGRDaGlsZChzY3JlZW4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7O0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/Main.js\n");

/***/ }),

/***/ "./src/assetdirectory.js":
/*!*******************************!*\
  !*** ./src/assetdirectory.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\nvar AssetDirectory = exports.AssetDirectory = {\n\t\"load\": [\"assets/fonts/Air Americana.ttf\", \"assets/fonts/AirAmericana.eot\", \"assets/fonts/AirAmericana.svg\", \"assets/fonts/AirAmericana.woff\", \"assets/fonts/AirAmericana.woff2\"],\n\t\"audio\": []\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXNzZXRkaXJlY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2Fzc2V0ZGlyZWN0b3J5LmpzPzZmZDEiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGxldCBBc3NldERpcmVjdG9yeSA9IHtcblx0XCJsb2FkXCI6IFtcblx0XHRcImFzc2V0cy9mb250cy9BaXIgQW1lcmljYW5hLnR0ZlwiLFxuXHRcdFwiYXNzZXRzL2ZvbnRzL0FpckFtZXJpY2FuYS5lb3RcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEuc3ZnXCIsXG5cdFx0XCJhc3NldHMvZm9udHMvQWlyQW1lcmljYW5hLndvZmZcIixcblx0XHRcImFzc2V0cy9mb250cy9BaXJBbWVyaWNhbmEud29mZjJcIlxuXHRdLFxuXHRcImF1ZGlvXCI6IFtdXG59OyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBT0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/assetdirectory.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nexports.App = undefined;\n\nvar _assetdirectory = __webpack_require__(/*! ./assetdirectory.js */ \"./src/assetdirectory.js\");\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nvar _Main = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n\nvar _Main2 = _interopRequireDefault(_Main);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar Config = __webpack_require__(/*! Config */ \"Config\");\n\nvar canvas = document.getElementById('game-canvas');\nvar pixiapp = new _pixi.Application({\n    view: canvas,\n    width: Config.BUILD.WIDTH,\n    height: Config.BUILD.HEIGHT\n});\n\ndocument.body.style.margin = \"0px\";\ndocument.body.style.overflow = \"hidden\";\n\n/*****************************************\r\n ************* ENTRY POINT ***************\r\n *****************************************/\nfunction ready(fn) {\n    if (document.readyState != 'loading') {\n        fn();\n    } else {\n        document.addEventListener('DOMContentLoaded', fn);\n    }\n}\n\nready(function () {\n    _Main2.default.start();\n});\n\nexports.App = pixiapp;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQXNzZXREaXJlY3RvcnkgfSBmcm9tICcuL2Fzc2V0ZGlyZWN0b3J5LmpzJztcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICdwaXhpLmpzJztcclxuaW1wb3J0IE1haW4gZnJvbSAnLi9NYWluLmpzJztcclxuXHJcbnZhciBDb25maWcgPSByZXF1aXJlKCdDb25maWcnKTtcclxuXHJcblxyXG5sZXQgY2FudmFzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2dhbWUtY2FudmFzJyk7XHJcbmxldCBwaXhpYXBwID0gbmV3IEFwcGxpY2F0aW9uKHtcclxuICAgIHZpZXc6IGNhbnZhcyxcclxuICAgIHdpZHRoOiBDb25maWcuQlVJTEQuV0lEVEgsXHJcbiAgICBoZWlnaHQ6IENvbmZpZy5CVUlMRC5IRUlHSFRcclxufSlcclxuXHJcblxyXG5kb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpbiA9IFwiMHB4XCI7XHJcbmRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAqKioqKioqKioqKioqIEVOVFJZIFBPSU5UICoqKioqKioqKioqKioqKlxyXG4gKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbmZ1bmN0aW9uIHJlYWR5KGZuKSB7XHJcbiAgICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSAnbG9hZGluZycpIHtcclxuICAgICAgICBmbigpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZm4pO1xyXG4gICAgfVxyXG59XHJcblxyXG5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBNYWluLnN0YXJ0KCk7XHJcbn0pO1xyXG5cclxuZXhwb3J0IHtwaXhpYXBwIGFzIEFwcH07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/screen/TitleScreen.js":
/*!***********************************!*\
  !*** ./src/screen/TitleScreen.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\n\nvar _pixi = __webpack_require__(/*! pixi.js */ \"./node_modules/pixi.js/lib/index.js\");\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar TitleScreen = function (_Container) {\n\t_inherits(TitleScreen, _Container);\n\n\tfunction TitleScreen() {\n\t\t_classCallCheck(this, TitleScreen);\n\n\t\tvar _this = _possibleConstructorReturn(this, (TitleScreen.__proto__ || Object.getPrototypeOf(TitleScreen)).call(this));\n\n\t\tvar app = new PIXI.Application({\n\t\t\t\"width\": 1000,\n\t\t\t\"height\": 800,\n\t\t\t\"view\": document.getElementById(\"game_canvas\"),\n\t\t\t\"backgroundColor\": 0x00FF00,\n\t\t\t\"autoStart\": true\n\t\t});\n\n\t\tvar idNumber = new _pixi.Text(\"11612727\", {\n\t\t\tfill: ['0xFFFFFF'],\n\t\t\tfontSize: 20\n\t\t});\n\t\tvar fullName = new _pixi.Text(\"Paolo V. Arevalo\", {\n\t\t\tfill: ['0x551a8b', '0x00FF00', '0x0000FF', '0xFFFF00'],\n\t\t\tfontSize: 20,\n\t\t\tfillGradientType: _pixi.TEXT_GRADIENT.LINEAR_HORIZONTAL\n\t\t});\n\t\tvar myProgram = new _pixi.Text(\"Bachelor of Science in Interactive Entertainment and Multimedia Computing Major in Game Development\", {\n\t\t\tfill: ['0xFF0000', '0x0000FF', '0xFFFF00'],\n\t\t\tfontSize: 18,\n\t\t\tfillGradientType: _pixi.TEXT_GRADIENT.LINEAR_VERTICAL,\n\t\t\tfillGradientStops: [0.08, 0.5]\n\t\t});\n\t\tvar myCourse = new _pixi.Text(\"Game Development for the Web\", {\n\t\t\tfill: ['white'],\n\t\t\tfontSize: 18,\n\t\t\twordWrap: true,\n\t\t\twordWrapWidth: 20\n\t\t});\n\t\tvar lastNumber = new _pixi.Text(\"I like to eat some Croissant\", {\n\t\t\tfill: ['white'],\n\t\t\tfontSize: 30,\n\t\t\tfontFamily: 'AirAmericana'\n\t\t});\n\t\tfullName.position.set(0, 20);\n\t\tmyProgram.position.set(0, 40);\n\t\tmyCourse.position.set(0, 60);\n\t\tlastNumber.position.set(0, 200);\n\n\t\tvar graphics = new PIXI.Graphics();\n\t\tvar point1 = new PIXI.Point(225, 475);\n\t\tvar point2 = new PIXI.Point(250, 400);\n\t\tvar point3 = new PIXI.Point(275, 475);\n\t\tvar point4 = new PIXI.Point(215, 420);\n\t\tvar point5 = new PIXI.Point(285, 420);\n\t\tvar point6 = new PIXI.Point(225, 475);\n\n\t\tvar points = [point1, point2, point3, point4, point5, point6];\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.beginFill(0x0000FF);\n\t\tgraphics.drawCircle(100, 100, 45);\n\t\tgraphics.endFill();\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.beginFill(0xFF0000);\n\t\tgraphics.drawRect(175, 100, 210, 150);\n\t\tgraphics.endFill();\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.beginFill(0x00FF00);\n\t\tgraphics.drawEllipse(500, 200, 75, 180);\n\t\tgraphics.endFill();\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.drawCircle(625, 100, 45);\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.drawRect(700, 100, 210, 150);\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.drawEllipse(100, 400, 75, 180);\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.moveTo(225, 275);\n\t\tgraphics.lineTo(325, 275);\n\t\tgraphics.lineTo(325, 375);\n\t\tgraphics.lineTo(225, 375);\n\t\tgraphics.lineTo(225, 275);\n\n\t\tgraphics.lineStyle(4, 0xFFFFFF);\n\t\tgraphics.drawPolygon(points);\n\n\t\t//Ears Left\n\t\tgraphics.lineStyle(4, 0x00FF00);\n\t\tgraphics.beginFill(0x00FF00);\n\t\tgraphics.drawEllipse(400, 500, 15, 50);\n\t\tgraphics.endFill();\n\t\t//Ears Right\n\t\tgraphics.lineStyle(4, 0x00FF00);\n\t\tgraphics.beginFill(0x00FF00);\n\t\tgraphics.drawEllipse(475, 500, 15, 50);\n\t\tgraphics.endFill();\n\t\t//Head\n\t\tgraphics.lineStyle(4, 0xFFFF00);\n\t\tgraphics.beginFill(0xFFFF00);\n\t\tgraphics.drawCircle(438, 585, 60);\n\t\tgraphics.endFill();\n\t\t//Eyes Left\n\t\tgraphics.lineStyle(4, 0xFF0000);\n\t\tgraphics.beginFill(0xFF0000);\n\t\tgraphics.drawCircle(415, 570, 8);\n\t\tgraphics.endFill();\n\t\t//Eyes Right\n\t\tgraphics.lineStyle(4, 0xFF0000);\n\t\tgraphics.beginFill(0xFF0000);\n\t\tgraphics.drawCircle(460, 570, 8);\n\t\tgraphics.endFill();\n\t\t//Mouth\n\t\tgraphics.lineStyle(4, 0x0000FF);\n\t\tgraphics.moveTo(400, 595);\n\t\tgraphics.lineTo(475, 595);\n\t\t//Teeth Left\n\t\tgraphics.lineStyle(4, 0x0000FF);\n\t\tgraphics.beginFill(0xFFFFFF);\n\t\tgraphics.drawRect(415, 595, 15, 20);\n\t\tgraphics.endFill();\n\t\t//Teeth Right\n\t\tgraphics.lineStyle(4, 0x0000FF);\n\t\tgraphics.beginFill(0xFFFFFF);\n\t\tgraphics.drawRect(445, 595, 15, 20);\n\t\tgraphics.endFill();\n\n\t\t_this.addChild(graphics);\n\n\t\t_this.addChild(idNumber);\n\t\t_this.addChild(fullName);\n\t\t_this.addChild(myProgram);\n\t\t_this.addChild(myCourse);\n\t\t_this.addChild(lastNumber);\n\n\t\tsetTimeout(function () {\n\t\t\tlastNumber.text = \"I like to eat some Croissant and some Baguette\";\n\t\t}, 1000);\n\n\t\treturn _this;\n\t}\n\n\treturn TitleScreen;\n}(_pixi.Container);\n\nexports.default = TitleScreen;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyZWVuL1RpdGxlU2NyZWVuLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zY3JlZW4vVGl0bGVTY3JlZW4uanM/YTM3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250YWluZXIsIFRleHQsVEVYVF9HUkFESUVOVCB9IGZyb20gXCJwaXhpLmpzXCI7XHJcblxyXG5jbGFzcyBUaXRsZVNjcmVlbiBleHRlbmRzIENvbnRhaW5lcntcclxuICAgIGNvbnN0cnVjdG9yKCl7XHJcblx0XHRzdXBlcigpO1xyXG5cdFx0XHJcblx0XHRjb25zdCBhcHAgPSBuZXcgUElYSS5BcHBsaWNhdGlvbih7XHJcblx0XHRcdFwid2lkdGhcIjoxMDAwLFxyXG5cdFx0XHRcImhlaWdodFwiOjgwMCxcclxuXHRcdFx0XCJ2aWV3XCI6IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZ2FtZV9jYW52YXNcIiksXHJcblx0XHRcdFwiYmFja2dyb3VuZENvbG9yXCI6IDB4MDBGRjAwLFxyXG5cdFx0XHRcImF1dG9TdGFydFwiOnRydWVcclxuXHRcdH0pO1xyXG5cdFx0XHJcblxyXG52YXIgaWROdW1iZXIgPSBuZXcgVGV4dChcIjExNjEyNzI3XCIse1xyXG5cdGZpbGw6IFsnMHhGRkZGRkYnXSxcclxuXHRmb250U2l6ZTogMjBcclxufSk7XHJcbnZhciBmdWxsTmFtZSA9IG5ldyBUZXh0KFwiUGFvbG8gVi4gQXJldmFsb1wiLHtcclxuXHRmaWxsOiBbJzB4NTUxYThiJywnMHgwMEZGMDAnLCcweDAwMDBGRicsJzB4RkZGRjAwJ10sXHJcblx0Zm9udFNpemU6IDIwLFxyXG5cdGZpbGxHcmFkaWVudFR5cGU6IFRFWFRfR1JBRElFTlQuTElORUFSX0hPUklaT05UQUxcclxufSk7XHJcbnZhciBteVByb2dyYW0gPSBuZXcgVGV4dChcIkJhY2hlbG9yIG9mIFNjaWVuY2UgaW4gSW50ZXJhY3RpdmUgRW50ZXJ0YWlubWVudCBhbmQgTXVsdGltZWRpYSBDb21wdXRpbmcgTWFqb3IgaW4gR2FtZSBEZXZlbG9wbWVudFwiLHtcclxuXHRmaWxsOiBbJzB4RkYwMDAwJywnMHgwMDAwRkYnLCcweEZGRkYwMCddLFxyXG5cdGZvbnRTaXplOiAxOCxcclxuXHRmaWxsR3JhZGllbnRUeXBlOiBURVhUX0dSQURJRU5ULkxJTkVBUl9WRVJUSUNBTCxcclxuXHRmaWxsR3JhZGllbnRTdG9wczogWzAuMDgsMC41XVxyXG59KTtcclxudmFyIG15Q291cnNlID0gbmV3IFRleHQoXCJHYW1lIERldmVsb3BtZW50IGZvciB0aGUgV2ViXCIse1xyXG5cdGZpbGw6IFsnd2hpdGUnXSxcclxuXHRmb250U2l6ZTogMTgsXHJcblx0d29yZFdyYXA6dHJ1ZSxcclxuXHR3b3JkV3JhcFdpZHRoOjIwXHJcbn0pO1xyXG52YXIgbGFzdE51bWJlciA9IG5ldyBUZXh0KFwiSSBsaWtlIHRvIGVhdCBzb21lIENyb2lzc2FudFwiLHtcclxuXHRmaWxsOiBbJ3doaXRlJ10sXHJcblx0Zm9udFNpemU6IDMwLFxyXG5cdGZvbnRGYW1pbHk6J0FpckFtZXJpY2FuYSdcclxufSk7XHJcbmZ1bGxOYW1lLnBvc2l0aW9uLnNldCgwLDIwKTtcclxubXlQcm9ncmFtLnBvc2l0aW9uLnNldCgwLDQwKTtcclxubXlDb3Vyc2UucG9zaXRpb24uc2V0KDAsNjApO1xyXG5sYXN0TnVtYmVyLnBvc2l0aW9uLnNldCgwLDIwMCk7XHJcblx0XHRcclxuXHRcdHZhciBncmFwaGljcyA9IG5ldyBQSVhJLkdyYXBoaWNzKCk7XHJcblx0XHR2YXIgcG9pbnQxID0gbmV3IFBJWEkuUG9pbnQoMjI1LDQ3NSk7XHJcblx0XHR2YXIgcG9pbnQyID0gbmV3IFBJWEkuUG9pbnQoMjUwLDQwMCk7XHJcblx0XHR2YXIgcG9pbnQzID0gbmV3IFBJWEkuUG9pbnQoMjc1LDQ3NSk7XHJcblx0XHR2YXIgcG9pbnQ0ID0gbmV3IFBJWEkuUG9pbnQoMjE1LDQyMCk7XHJcblx0XHR2YXIgcG9pbnQ1ID0gbmV3IFBJWEkuUG9pbnQoMjg1LDQyMCk7XHJcblx0XHR2YXIgcG9pbnQ2ID0gbmV3IFBJWEkuUG9pbnQoMjI1LDQ3NSk7XHJcblx0XHRcclxuXHRcdHZhciBwb2ludHMgPSBbcG9pbnQxLHBvaW50Mixwb2ludDMscG9pbnQ0LHBvaW50NSxwb2ludDZdO1xyXG5cdFx0XHJcblx0XHRncmFwaGljcy5saW5lU3R5bGUoNCwweEZGRkZGRik7XHJcblx0XHRncmFwaGljcy5iZWdpbkZpbGwoMHgwMDAwRkYpO1xyXG5cdFx0Z3JhcGhpY3MuZHJhd0NpcmNsZSgxMDAsMTAwLDQ1KTtcclxuXHRcdGdyYXBoaWNzLmVuZEZpbGwoKTtcclxuXHRcdFxyXG5cdFx0Z3JhcGhpY3MubGluZVN0eWxlKDQsMHhGRkZGRkYpO1xyXG5cdFx0Z3JhcGhpY3MuYmVnaW5GaWxsKDB4RkYwMDAwKTtcclxuXHRcdGdyYXBoaWNzLmRyYXdSZWN0KDE3NSwxMDAsMjEwLDE1MCk7XHJcblx0XHRncmFwaGljcy5lbmRGaWxsKCk7XHJcblx0XHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkZGRkZGKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweDAwRkYwMCk7XHJcblx0XHRncmFwaGljcy5kcmF3RWxsaXBzZSg1MDAsMjAwLDc1LDE4MCk7XHJcblx0XHRncmFwaGljcy5lbmRGaWxsKCk7XHJcblx0XHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkZGRkZGKTtcclxuXHRcdGdyYXBoaWNzLmRyYXdDaXJjbGUoNjI1LDEwMCw0NSk7XHJcblx0XHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkZGRkZGKTtcclxuXHRcdGdyYXBoaWNzLmRyYXdSZWN0KDcwMCwxMDAsMjEwLDE1MCk7XHJcblx0XHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkZGRkZGKTtcclxuXHRcdGdyYXBoaWNzLmRyYXdFbGxpcHNlKDEwMCw0MDAsNzUsMTgwKTtcclxuXHRcdFxyXG5cdFx0Z3JhcGhpY3MubGluZVN0eWxlKDQsMHhGRkZGRkYpO1xyXG5cdFx0Z3JhcGhpY3MubW92ZVRvKDIyNSwyNzUpO1xyXG5cdFx0Z3JhcGhpY3MubGluZVRvKDMyNSwyNzUpO1xyXG5cdFx0Z3JhcGhpY3MubGluZVRvKDMyNSwzNzUpO1xyXG5cdFx0Z3JhcGhpY3MubGluZVRvKDIyNSwzNzUpO1xyXG5cdFx0Z3JhcGhpY3MubGluZVRvKDIyNSwyNzUpO1xyXG5cdFx0XHJcblx0XHRncmFwaGljcy5saW5lU3R5bGUoNCwweEZGRkZGRik7XHJcblx0XHRncmFwaGljcy5kcmF3UG9seWdvbihwb2ludHMpO1xyXG5cdFx0XHJcblx0XHQvL0VhcnMgTGVmdFxyXG5cdFx0Z3JhcGhpY3MubGluZVN0eWxlKDQsMHgwMEZGMDApO1xyXG5cdFx0Z3JhcGhpY3MuYmVnaW5GaWxsKDB4MDBGRjAwKTtcclxuXHRcdGdyYXBoaWNzLmRyYXdFbGxpcHNlKDQwMCw1MDAsMTUsNTApO1xyXG5cdFx0Z3JhcGhpY3MuZW5kRmlsbCgpO1xyXG5cdFx0Ly9FYXJzIFJpZ2h0XHJcblx0XHRncmFwaGljcy5saW5lU3R5bGUoNCwweDAwRkYwMCk7XHJcblx0XHRncmFwaGljcy5iZWdpbkZpbGwoMHgwMEZGMDApO1xyXG5cdFx0Z3JhcGhpY3MuZHJhd0VsbGlwc2UoNDc1LDUwMCwxNSw1MCk7XHJcblx0XHRncmFwaGljcy5lbmRGaWxsKCk7XHJcblx0XHQvL0hlYWRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkZGRjAwKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGRkYwMCk7XHJcblx0XHRncmFwaGljcy5kcmF3Q2lyY2xlKDQzOCw1ODUsNjApO1xyXG5cdFx0Z3JhcGhpY3MuZW5kRmlsbCgpO1xyXG5cdFx0Ly9FeWVzIExlZnRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkYwMDAwKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGMDAwMCk7XHJcblx0XHRncmFwaGljcy5kcmF3Q2lyY2xlKDQxNSw1NzAsOCk7XHJcblx0XHRncmFwaGljcy5lbmRGaWxsKCk7XHJcblx0XHQvL0V5ZXMgUmlnaHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4RkYwMDAwKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGMDAwMCk7XHJcblx0XHRncmFwaGljcy5kcmF3Q2lyY2xlKDQ2MCw1NzAsOCk7XHJcblx0XHRncmFwaGljcy5lbmRGaWxsKCk7XHJcblx0XHQvL01vdXRoXHJcblx0XHRncmFwaGljcy5saW5lU3R5bGUoNCwweDAwMDBGRik7XHJcblx0XHRncmFwaGljcy5tb3ZlVG8oNDAwLDU5NSk7XHJcblx0XHRncmFwaGljcy5saW5lVG8oNDc1LDU5NSk7XHJcblx0XHQvL1RlZXRoIExlZnRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4MDAwMEZGKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGRkZGRik7XHJcblx0XHRncmFwaGljcy5kcmF3UmVjdCg0MTUsNTk1LDE1LDIwKTtcclxuXHRcdGdyYXBoaWNzLmVuZEZpbGwoKTtcclxuXHRcdC8vVGVldGggUmlnaHRcclxuXHRcdGdyYXBoaWNzLmxpbmVTdHlsZSg0LDB4MDAwMEZGKTtcclxuXHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGRkZGRik7XHJcblx0XHRncmFwaGljcy5kcmF3UmVjdCg0NDUsNTk1LDE1LDIwKTtcclxuXHRcdGdyYXBoaWNzLmVuZEZpbGwoKTtcclxuXHRcdFxyXG5cdFx0XHJcblx0XHR0aGlzLmFkZENoaWxkKGdyYXBoaWNzKTtcclxuXHJcblx0XHRcdFx0dGhpcy5hZGRDaGlsZChpZE51bWJlcik7XHJcblx0XHRcdFx0dGhpcy5hZGRDaGlsZChmdWxsTmFtZSk7XHJcblx0XHRcdFx0dGhpcy5hZGRDaGlsZChteVByb2dyYW0pO1xyXG5cdFx0XHRcdHRoaXMuYWRkQ2hpbGQobXlDb3Vyc2UpO1xyXG5cdFx0XHRcdHRoaXMuYWRkQ2hpbGQobGFzdE51bWJlcik7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c2V0VGltZW91dCgoKT0+e1xyXG4gICAgICAgIFx0bGFzdE51bWJlci50ZXh0ID0gXCJJIGxpa2UgdG8gZWF0IHNvbWUgQ3JvaXNzYW50IGFuZCBzb21lIEJhZ3VldHRlXCI7XHJcblx0XHR9LDEwMDApO1xyXG5cdFx0XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFRpdGxlU2NyZWVuOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7QUFDQTs7Ozs7OztBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0lBO0FBNElBO0FBQ0E7O0FBOUlBO0FBQ0E7QUErSUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/screen/TitleScreen.js\n");

/***/ }),

/***/ "Config":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** external "{\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}}" ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = {\"BUILD\":{\"GAME_TITLE\":\"GAME TITLE\",\"VERSION\":\"0.0.1\",\"WIDTH\":889,\"HEIGHT\":500,\"MINIFIED_EXTERNAL_JS\":false,\"MINIFIED_EXTERNAL_JS_PATH\":\"./src/external/minified\",\"UNMINIFIED_EXTERNAL_JS_PATH\":\"./src/external/unminified\",\"EXTERNAL_JS\":[\"soundjs.js\",\"preloadjs.js\"],\"ASSETPATH\":{\"load\":\"assets\",\"audio\":\"assets/audio\"}}};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ29uZmlnLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwie1xcXCJCVUlMRFxcXCI6e1xcXCJHQU1FX1RJVExFXFxcIjpcXFwiR0FNRSBUSVRMRVxcXCIsXFxcIlZFUlNJT05cXFwiOlxcXCIwLjAuMVxcXCIsXFxcIldJRFRIXFxcIjo4ODksXFxcIkhFSUdIVFxcXCI6NTAwLFxcXCJNSU5JRklFRF9FWFRFUk5BTF9KU1xcXCI6ZmFsc2UsXFxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcXFwiOlxcXCIuL3NyYy9leHRlcm5hbC9taW5pZmllZFxcXCIsXFxcIlVOTUlOSUZJRURfRVhURVJOQUxfSlNfUEFUSFxcXCI6XFxcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcXFwiLFxcXCJFWFRFUk5BTF9KU1xcXCI6W1xcXCJzb3VuZGpzLmpzXFxcIixcXFwicHJlbG9hZGpzLmpzXFxcIl0sXFxcIkFTU0VUUEFUSFxcXCI6e1xcXCJsb2FkXFxcIjpcXFwiYXNzZXRzXFxcIixcXFwiYXVkaW9cXFwiOlxcXCJhc3NldHMvYXVkaW9cXFwifX19XCI/ZDE0YiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHtcIkJVSUxEXCI6e1wiR0FNRV9USVRMRVwiOlwiR0FNRSBUSVRMRVwiLFwiVkVSU0lPTlwiOlwiMC4wLjFcIixcIldJRFRIXCI6ODg5LFwiSEVJR0hUXCI6NTAwLFwiTUlOSUZJRURfRVhURVJOQUxfSlNcIjpmYWxzZSxcIk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL21pbmlmaWVkXCIsXCJVTk1JTklGSUVEX0VYVEVSTkFMX0pTX1BBVEhcIjpcIi4vc3JjL2V4dGVybmFsL3VubWluaWZpZWRcIixcIkVYVEVSTkFMX0pTXCI6W1wic291bmRqcy5qc1wiLFwicHJlbG9hZGpzLmpzXCJdLFwiQVNTRVRQQVRIXCI6e1wibG9hZFwiOlwiYXNzZXRzXCIsXCJhdWRpb1wiOlwiYXNzZXRzL2F1ZGlvXCJ9fX07Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///Config\n");

/***/ })

/******/ });